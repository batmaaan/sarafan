package com.example.andrey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
// https://gitlab.com/aa7435178/sarafan

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
